/**
 * 
 */
package earthnewmedia.gradle.daemon.exec

import javax.inject.Inject

import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.Property
import org.gradle.api.tasks.AbstractExecTask
import org.gradle.api.tasks.TaskAction
import org.gradle.process.ExecResult
import org.gradle.process.ExecSpec
import org.gradle.process.internal.ExecHandle

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class DaemonExec extends AbstractExecTask<DaemonExec> implements ExecSpec {

    private final DaemonExecActionFactory execActionFactory = objectFactory.newInstance(DaemonExecActionFactory)

    private final DaemonExecAction execAction = execActionFactory.newDecoratedDaemonExecAction()

    private final Property<ExecResult> execResult = objectFactory.property(ExecResult)

    public DaemonExec() {
        super(DaemonExec)

        setExecAction(execAction)
    }

    @Inject
    protected ObjectFactory getObjectFactory() {
        throw new UnsupportedOperationException()
    }
    
    void processReadyWhen(Closure<Boolean> closure) {
        execAction.readyWhen(closure)
    }

    @TaskAction
    protected void exec() {
        execAction.setExecutable(this.getExecutable())
        execAction.setArgs(this.getArgs())
        execAction.setWorkingDir(this.getWorkingDir())
        execAction.setEnvironment(this.getEnvironment())
        
        execResult.set(execAction.execute())
    }
}