/**
 * 
 */
package earthnewmedia.gradle.daemon.exec

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
import org.gradle.api.NonExtensible
import org.gradle.process.ExecResult
import org.gradle.process.internal.ExecAction

@NonExtensible
interface DaemonExecAction extends ExecAction  {
    void readyWhen(Closure<ExecResult> closure)
}
