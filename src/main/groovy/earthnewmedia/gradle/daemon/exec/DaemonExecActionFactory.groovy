/**
 * 
 */
package earthnewmedia.gradle.daemon.exec

import java.util.concurrent.Executor

import javax.inject.Inject

import org.gradle.api.internal.file.FileCollectionFactory
import org.gradle.api.internal.file.FileResolver
import org.gradle.api.model.ObjectFactory
import org.gradle.initialization.BuildCancellationToken
import org.gradle.initialization.DefaultBuildCancellationToken
import org.gradle.internal.concurrent.ExecutorFactory

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class DaemonExecActionFactory {

    final FileResolver fileResolver
    final Executor executor
    final BuildCancellationToken buildCancellationToken
    final ObjectFactory objectFactory

    @Inject
    DaemonExecActionFactory(FileResolver fileResolver, ExecutorFactory executorFactory, ObjectFactory objectFactory) {
        this.fileResolver = fileResolver
        this.executor = executorFactory.create("DaemonExec process")
        this.buildCancellationToken = new DefaultBuildCancellationToken()
        this.objectFactory = objectFactory
    }

    DaemonExecAction newDecoratedDaemonExecAction() {
        return objectFactory.newInstance(DefaultDaemonExecAction, fileResolver, executor, buildCancellationToken)
    }
}
