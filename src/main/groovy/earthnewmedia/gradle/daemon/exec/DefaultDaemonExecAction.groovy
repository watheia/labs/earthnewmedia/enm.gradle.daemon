/**
 * 
 */
package earthnewmedia.gradle.daemon.exec

import java.util.concurrent.Executor
import java.util.concurrent.locks.Condition
import java.util.concurrent.locks.ReentrantLock

import javax.inject.Inject

import org.gradle.initialization.BuildCancellationToken
import org.gradle.internal.UncheckedException
import org.gradle.internal.file.PathToFileResolver
import org.gradle.process.ExecResult
import org.gradle.process.internal.DefaultExecHandleBuilder
import org.gradle.process.internal.ExecAction
import org.gradle.process.internal.ExecHandle

/**
 * @author Aaron R Miller<aaron.miller@waweb.io>
 *
 */
class DefaultDaemonExecAction extends DefaultExecHandleBuilder implements ExecAction, DaemonExecAction {

    private static ExecResult waitForReady(final ExecHandle execHandle, final Closure<Boolean> readyCondition = null) {
        if(readyCondition) {
            final ReentrantLock lock = new ReentrantLock()
            final Condition stateChanged = lock.newCondition()

            lock.lock()
            try {
                //TODO stream stdout to readyCondition, unlock when returns true
                while (!execHandle.getState().isTerminal()) {
                    try {
                        stateChanged.await()
                    } catch (InterruptedException e) {
                        execHandle.abort()
                        throw UncheckedException.throwAsUncheckedException(e)
                    }
                }
            } finally {
                lock.unlock()
            }

            //TODO we need to figure out how to return a valid 
            //     ExecResult which has not yet exited
        }

        return execHandle.start().waitForFinish()
    }

    private Closure<Boolean> readyCondition = null

    @Inject
    DefaultDaemonExecAction(PathToFileResolver fileResolver, Executor executor, BuildCancellationToken buildCancellationToken) {
        super(fileResolver, executor, buildCancellationToken)
    }

    @Override
    public ExecResult execute() {
        final ExecHandle execHandle = build()
        final ExecResult execResult = waitForReady(execHandle, readyCondition)
        // TODO defer assertion until process exits
        if (!isIgnoreExitValue()) {
            execResult.assertNormalExitValue()
        }
        return execResult
    }

    @Override
    void readyWhen(Closure<Boolean> closure) {
        readyCondition = closure
    }
}
